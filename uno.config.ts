import { defineConfig, presetUno } from "unocss"
import presetIcons from "@unocss/preset-icons"
import transformerDirectives from "@unocss/transformer-directives"

export default defineConfig({
    theme: {
        colors: {
            "background": "hsl(0 0% 100%)",
            "foreground": "hsl(222.2 47.4% 11.2%)",
            "muted": "hsl(210 40% 96.1%)",
            "muted-foreground": "hsl(215.4 16.3% 56.9%)",
            "popover": "hsl(0 0% 100%)",
            "popover-foreground": "hsl(222.2 47.4% 11.2%)",
            "border": "hsl(214.3 31.8% 91.4%)",
            "input": "hsl(214.3 31.8% 91.4%)",
            "card": "hsl(0 0% 100%)",
            "card-foreground": "hsl(222.2 47.4% 11.2%)",
            "primary": {
                DEFAULT: "hsl(222.2 47.4% 11.2%)",
                foreground: "hsl(210 40% 98%)",
            },
            "secondary": {
                DEFAULT: "hsl(210 40% 96.1%)",
                foreground: "hsl(222.2 47.4% 11.2%)",
            },
            "accent": {
                DEFAULT: "hsl(210 40% 96.1%)",
                foreground: "hsl(222.2 47.4% 11.2%)",
            },
            "destructive": {
                DEFAULT: "hsl(0 100% 50%)",
                foreground: "hsl(210 40% 98%)",
            },
            "ring": "hsl(215 20.2% 65.1%)",
        },
    },
    presets: [presetIcons(), presetUno()],
    transformers: [transformerDirectives()],
})
