import { cn } from "../lib/utils"
import React from "react"

interface TreeType {
    depth: number
    slug: string
    text: string
}

interface TreeProps {
    tree: TreeType[]
    activeItem: string
}

export function DashboardTableOfContents({ tree }: { tree: TreeType[] }) {
    const activeHeading = useActiveItem(tree)

    return (
        <div className="space-y-2">
            <p className="font-medium">On This Page</p>
            <Tree tree={tree} activeItem={activeHeading} />
        </div>
    )
}

function useActiveItem(tree: TreeType[]) {
    const [activeId, setActiveId] = React.useState("")

    React.useEffect(() => {
        const observer = new IntersectionObserver(
            (entries) => {
                entries.forEach((entry) => {
                    if (entry.isIntersecting) {
                        setActiveId(entry.target.id)
                    }
                })
            },
            { rootMargin: `0% 0% -80% 0%` }
        )

        tree?.forEach((id) => {
            const element = document.getElementById(id.slug)
            if (element) {
                observer.observe(element)
            }
        })

        return () => {
            tree?.forEach((id) => {
                const element = document.getElementById(id.slug)
                if (element) {
                    observer.unobserve(element)
                }
            })
        }
    }, [tree])

    return activeId
}

export function Tree({ tree, activeItem }: TreeProps) {
    return (
        <ul className={cn("m-0 list-none")}>
            {tree.map((item, index) => {
                const { depth, slug, text } = item

                return (
                    <li
                        key={index}
                        className={cn("mt-0 pt-2", {
                            "pl-4": depth === 3,
                            "pl-8": depth === 4,
                        })}>
                        <a
                            href={`#${slug}`}
                            className={cn(
                                "inline-block no-underline transition-colors hover:text-foreground",
                                slug === activeItem
                                    ? "font-medium text-foreground"
                                    : "text-muted-foreground"
                            )}>
                            {text}
                        </a>
                    </li>
                )
            })}
        </ul>
    )
}
