import { siteConfig } from "../lib/site-config"
import {
    NavigationMenu,
    NavigationMenuContent,
    NavigationMenuItem,
    NavigationMenuLink,
    NavigationMenuList,
    NavigationMenuTrigger,
    navigationMenuTriggerStyle,
} from "./ui/navigation-menu"
import { cn } from "../lib/utils"

export function MainNav() {
    return (
        <div className="mr-4 flex">
            <a href="/lethbridge/" className="mr-6 flex items-center space-x-2">
                {/* <Icons.logo className="h-6 w-12" /> */}
                <span className="font-bold">Club Squared</span>
            </a>
            <NavigationMenu className="hidden md:block">
                <NavigationMenuList>
                    {siteConfig.mainNav.map((item) => (
                        <>
                            {item.items && (
                                <NavigationMenuItem>
                                    <NavigationMenuTrigger>
                                        {item.title}
                                    </NavigationMenuTrigger>
                                    <NavigationMenuContent>
                                        <ul className="grid w-[400px] gap-1 p-4 md:w-[500px] md:grid-cols-2 lg:w-[600px] ">
                                            {item.items.map((subItem) => (
                                                <li key={subItem.title}>
                                                    <NavigationMenuLink asChild>
                                                        <a
                                                            href={subItem.href}
                                                            className={cn(
                                                                "block select-none space-y-1 cursor-pointer rounded-md p-3 leading-none no-underline outline-none transition-colors",
                                                                {
                                                                    "!text-muted-foreground/50 cursor-not-allowed ":
                                                                        subItem.disabled,
                                                                    "hover:bg-muted hover:text-foreground focus:bg-muted focus:text-foreground":
                                                                        !subItem.disabled,
                                                                }
                                                            )}>
                                                            <div className="text-sm font-medium leading-none">
                                                                {subItem.title}
                                                            </div>
                                                            <p
                                                                className={cn(
                                                                    "line-clamp-2 text-sm leading-snug text-muted-foreground",
                                                                    {
                                                                        "!text-muted-foreground/50":
                                                                            subItem.disabled,
                                                                    }
                                                                )}>
                                                                {
                                                                    subItem.description
                                                                }
                                                            </p>
                                                        </a>
                                                    </NavigationMenuLink>
                                                </li>
                                            ))}
                                        </ul>
                                    </NavigationMenuContent>
                                </NavigationMenuItem>
                            )}
                            {!item.items && (
                                <NavigationMenuItem>
                                    <NavigationMenuLink
                                        href={item.href}
                                        className={navigationMenuTriggerStyle()}>
                                        {item.title}
                                    </NavigationMenuLink>
                                </NavigationMenuItem>
                            )}
                        </>
                    ))}
                </NavigationMenuList>
            </NavigationMenu>
        </div>
    )
}
