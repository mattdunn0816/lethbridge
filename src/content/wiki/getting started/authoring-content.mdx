---
title: Authoring Content in Markdown
description: An overview of the Markdown syntax Starlight supports.
sidebar:
    order: 2
---

import Aside from "../../../components/markdown/aside.astro"

Starlight supports the full range of [Markdown](https://daringfireball.net/projects/markdown/) syntax in `.md` files as well as frontmatter [YAML](https://dev.to/paulasantamaria/introduction-to-yaml-125f) to define metadata such as a title and description.

Please be sure to check the [MDX docs](https://mdxjs.com/docs/what-is-mdx/#markdown) or [Markdoc docs](https://markdoc.dev/docs/syntax) if using those file formats, as Markdown support and usage can differ.

## Inline styles

Text can be **bold**, _italic_, or ~~strikethrough~~.

```md
Text can be **bold**, _italic_, or ~~strikethrough~~.
```

You can [link to another page](/getting-started/).

```md
You can [link to another page](/getting-started/).
```

You can highlight `inline code` with backticks.

```md
You can highlight `inline code` with backticks.
```

## Images

Images in Starlight use [Astro’s built-in optimized asset support](https://docs.astro.build/en/guides/assets/).

Markdown and MDX support the Markdown syntax for displaying images that includes alt-text for screen readers and assistive technology.

![An illustration of planets and stars featuring the word “astro”](https://raw.githubusercontent.com/withastro/docs/main/public/default-og-image.png)

```md
![An illustration of planets and stars featuring the word “astro”](https://raw.githubusercontent.com/withastro/docs/main/public/default-og-image.png)
```

Relative image paths are also supported for images stored locally in your project.

```md
// src/content/docs/page-1.md

![A rocketship in space](../../assets/images/rocket.svg)
```

## Headings

You can structure content using a heading. Headings in Markdown are indicated by a number of `#` at the start of the line.

### How to structure page content in Starlight

Starlight is configured to automatically use your page title as a top-level heading and will include an "Overview" heading at top of each page's table of contents. We recommend starting each page with regular paragraph text content and using on-page headings from `<h2>` and down:

```md
---
title: Markdown Guide
description: How to use Markdown in Starlight
---

This page describes how to use Markdown in Starlight.

## Inline Styles

## Headings
```

### Automatic heading anchor links

Using headings in Markdown will automatically give you anchor links so you can link directly to certain sections of your page:

```md
---
title: My page of content
description: How to use Starlight's built-in anchor links
---

## Introduction

I can link to [my conclusion](#conclusion) lower on the same page.

## Conclusion

`https://my-site.com/page1/#introduction` navigates directly to my Introduction.
```

Level 2 (`<h2>`) and Level 3 (`<h3>`) headings will automatically appear in the page table of contents.

## Tables

Tables are widely used in documentation and are **100%** supported by the wiki. You can create a table by using the following syntax:

| Column 1 | Column 2 | Column 3 |
| -------- | -------- | -------- |
| R1C1     | R1C2     | R1C3     |
| R2C1     | R2C2     | R2C3     |
| R3C1     | R3C2     | R3C3     |
| R4C1     | R4C2     | R4C3     |

```md
| Column 1 | Column 2 | Column 3 |
| -------- | -------- | -------- |
| R1C1     | R1C2     | R1C3     |
| R2C1     | R2C2     | R2C3     |
| R3C1     | R3C2     | R3C3     |
| R4C1     | R4C2     | R4C3     |
```

## Asides

Asides (also known as “admonitions” or “callouts”) are useful for displaying secondary information alongside a page’s main content.

Starlight provides a custom Markdown syntax for rendering asides. Aside blocks are indicated using a pair of triple colons `:::` to wrap your content, and can be of type `note`, `tip`, `caution` or `danger`.

You can nest any other Markdown content types inside an aside, but asides are best suited to short and concise chunks of content.

### Note aside

<Aside variant="note" title="Note">
   
    Starlight is a documentation website toolkit built with [Astro](https://astro.build/). You can get started with this command:

    ```sh
    npm create astro@latest -- --template starlight
    ```

</Aside>

````mdx
<Aside variant="note" title="Note">

    Starlight is a documentation website toolkit built with [Astro](https://astro.build/). You can get started with this command:

    ```sh
    npm create astro@latest -- --template starlight
    ```

</Aside>
````

### Custom aside titles

You can specify a custom title for the aside in square brackets following the aside type.

<Aside variant="tip" title="Did you know?">
    Astro helps you build faster websites with [“Islands
    Architecture”](https://docs.astro.build/en/concepts/islands/).
</Aside>

```mdx
<Aside variant="tip" title="Did you know?">
    Astro helps you build faster websites with [“Islands
    Architecture”](https://docs.astro.build/en/concepts/islands/).
</Aside>
```

### More aside types

Caution and danger asides are helpful for drawing a user’s attention to details that may trip them up.
If you find yourself using these a lot, it may also be a sign that the thing you are documenting could benefit from being redesigned.

<Aside variant="caution" title="Caution">

If you are not sure you want an awesome docs site, think twice before using [Starlight](../../).

</Aside>

<Aside variant="danger" title="Danger">

Your users may be more productive and find your product easier to use thanks to helpful Starlight features.

-   Clear navigation
-   User-configurable colour theme
-   [i18n support](/guides/i18n)

</Aside>

```mdx
<Aside variant="caution" title="Caution">

If you are not sure you want an awesome docs site, think twice before using [Starlight](../../).

</Aside>

<Aside variant="danger" title="Danger">

Your users may be more productive and find your product easier to use thanks to helpful Starlight features.

-   Clear navigation
-   User-configurable colour theme
-   [i18n support](/guides/i18n)

</Aside>
```

## Blockquotes

> This is a blockquote, which is commonly used when quoting another person or document.
>
> Blockquotes are indicated by a `>` at the start of each line.

```md
> This is a blockquote, which is commonly used when quoting another person or document.
>
> Blockquotes are indicated by a `>` at the start of each line.
```

## Lists

Lists are also supported by the wiki. Both ordered lists as well as unordered lists are supported. You can create a list by using the following syntax:

### Ordered list

1. Item 1
2. Item 2
3. Item 3

### Unordered list

-   Item 1
-   Item 2
-   Item 3

```md
### Ordered list

1. Item 1
2. Item 2
3. Item 3

### Unordered list

-   Item 1
-   Item 2
-   Item 3
```

## Code blocks

A code block is indicated by a block with three backticks <code>```</code> at the start and end. You can indicate the programming language being used after the opening backticks.

```js
// Javascript code with syntax highlighting.
var fun = function lang(l) {
    dateformat.i18n = require("./lang/" + l)
    return true
}
```

````md
```js
// Javascript code with syntax highlighting.
var fun = function lang(l) {
    dateformat.i18n = require("./lang/" + l)
    return true
}
```
````

```md
Long, single-line code blocks should not wrap. They should horizontally scroll if they are too long. This line should be long enough to demonstrate this.
```

## Other common Markdown features

Starlight supports all other Markdown authoring syntax, such as lists and tables. See the [Markdown Cheat Sheet from The Markdown Guide](https://www.markdownguide.org/cheat-sheet/) for a quick overview of all the Markdown syntax elements.
