---
title: Components
description: Using components to build your application
---

Components let you easily reuse a piece of UI or styling consistently.
Examples might include a link card or a YouTube embed.
Starlight supports the use of components in [MDX](https://mdxjs.com/) files and provides some common components for you to use.

[Learn more about building components in the Astro Docs](https://docs.astro.build/en/core-concepts/astro-components/).

## Using a component

You can use a component by importing it into your MDX file and then rendering it as a JSX tag.
These look like HTML tags but start with an uppercase letter matching the name in your `import` statement:

```mdx
---
# src/content/wiki/index.mdx
title: Welcome to my docs
---

import SomeComponent from "../../components/SomeComponent.astro"
import AnotherComponent from "../../components/AnotherComponent.astro"

<SomeComponent prop="something" />

<AnotherComponent>
    Components can also contain **nested content**.
</AnotherComponent>
```

## Built-in components

Starlight provides some built-in components for common documentation use cases.
These components are available from the `@astrojs/starlight/components` package.

### Cards

import Card from "../../../components/markdown/card.astro"
import Grid from "../../../components/markdown/grid.astro"
import LinkCard from "../../../components/markdown/link-card.astro"

You can display content in a box matching Starlight’s styles using the `<Card>` component.
Wrap multiple cards in the `<CardGrid>` component to display cards side-by-side when there’s enough space.

```mdx
import Card from "../../../components/markdown/card.astro"
import CardGrid from "../../../components/markdown/grid.astro"

<Card title="Check this out">Interesting content you want to highlight.</Card>

<CardGrid>
    <Card title="Stars" icon="star">
        Sirius, Vega, Betelgeuse
    </Card>
    <Card title="Moons" icon="moon">
        Io, Europa, Ganymede
    </Card>
</CardGrid>
```

The code above generates the following on the page:

<Card title="Check this out">Interesting content you want to highlight.</Card>

<Grid>
    <Card title="Stars" icon="star">
        Sirius, Vega, Betelgeuse
    </Card>
    <Card title="Moons" icon="moon">
        Io, Europa, Ganymede
    </Card>
</Grid>

### Link Cards

Use the `<LinkCard>` component to link prominently to different pages.

A `<LinkCard>` requires a `title` and an [`href`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a#href) attribute. You can optionally include a short `description` or other link attributes such as `target`.

Group multiple `<LinkCard>` components in `<CardGrid>` to display cards side-by-side when there’s enough space.

```mdx
import CardGrid from "../../../components/markdown/grid.astro"
import LinkCard from "../../../components/markdown/link-card.astro"

<LinkCard
    title="Customizing Starlight"
    description="Learn how to make your Starlight site your own with custom styles, fonts, and more."
    href="/guides/customization/"
/>

<CardGrid>
    <LinkCard title="Authoring Markdown" href="/guides/authoring-content/" />
    <LinkCard title="Components" href="/guides/components/" />
</CardGrid>
```

The above code generates the following on the page:

<LinkCard
    title="Customizing Starlight"
    description="Learn how to make your Starlight site your own with custom styles, fonts, and more."
    href="/guides/customization/"
/>

<Grid>
    <LinkCard title="Authoring Markdown" href="/guides/authoring-content/" />
    <LinkCard title="Components" href="/guides/components/" />
</Grid>

### Grid

You can also you the `<Grid>` component independently of `<Card>` to display content side-by-side when there’s enough space.

```mdx
import Grid from "../../../components/markdown/grid.astro"

<Grid>
     Markdown and MDX support the Markdown syntax for displaying images that includes alt-text for screen readers and assistive technology.

    ![An illustration of planets and stars featuring the word “astro”](https://raw.githubusercontent.com/withastro/docs/main/public/default-og-image.png)

    ![An illustration of planets and stars featuring the word “astro”](https://raw.githubusercontent.com/withastro/docs/main/public/default-og-image.png)

     Markdown and MDX support the Markdown syntax for displaying images that includes alt-text for screen readers and assistive technology.

</Grid>
```

The above code generates the following on the page:

<Grid center>
    Markdown and MDX support the Markdown syntax for displaying images that includes alt-text for screen readers and assistive technology.

    ![An illustration of planets and stars featuring the word “astro”](https://raw.githubusercontent.com/withastro/docs/main/public/default-og-image.png)

    ![An illustration of planets and stars featuring the word “astro”](https://raw.githubusercontent.com/withastro/docs/main/public/default-og-image.png)

     Markdown and MDX support the Markdown syntax for displaying images that includes alt-text for screen readers and assistive technology.

</Grid>

### Steps

import Steps from "../../../components/markdown/steps.astro"

Steps which are are a great way to break up your documentation into steps. There are two ways you can use steps:

**Steps with headings**

Steps with headings will display numbers

<Steps>

    #### Step 1

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu mauris diam. Quisque vel pellentesque lectus. Morbi bibendum aliquet lectus, ut hendrerit leo accumsan et.

    #### Step 2

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu mauris diam. Quisque vel pellentesque lectus. Morbi bibendum aliquet lectus, ut hendrerit leo accumsan et. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu mauris diam. Quisque vel pellentesque lectus. Morbi bibendum aliquet lectus, ut hendrerit leo accumsan et.

    #### Step 3

    Lorem ipsum dolor sit amet, consectetur adipiscing elit.

</Steps>

**Steps without headings**

Steps without headings will just shift everything to the right and not display numbers

<Steps>

    <div className="mb-4" />

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu mauris diam. Quisque vel pellentesque lectus. Morbi bibendum aliquet lectus, ut hendrerit leo accumsan et.

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu mauris diam. Quisque vel pellentesque lectus. Morbi bibendum aliquet lectus, ut hendrerit leo accumsan et. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu mauris diam. Quisque vel pellentesque lectus. Morbi bibendum aliquet lectus, ut hendrerit leo accumsan et.

    Lorem ipsum dolor sit amet, consectetur adipiscing elit.

</Steps>
