---
title: Introduction
description: Getting started with the UofL iGem wiki.
sidebar:
    order: 1
---

Welcome to the uLeth iGem Template! Here you will find information on how to use the wiki and what you can use to make the wiki great. If you are new to the wiki, this is the **perfect** place to be as we will guide you through our entire project step by step.

If you are not new to the wiki or would just like to explore at your own pace feel free to use the navigation bar on the left to explore at your lesiure.

_Now lets talk about the features of this wiki._
