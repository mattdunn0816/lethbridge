---
title: Project Description
description: Project section of the wiki
slug: description
---

Clubroot is a soil borne pathogen that causes the development of galls on the roots of brassicae crops. Further manifestations of the disease are characterized by swollen roots and hypocotyl deformation resulting in clubbed roots. Clubbed root formation and disease progression depletes essential nutrients typically used for plant growth and survival, while infected plants demonstrate reduced aboveground growth, premature organ ripening, and shriveled seeds in severe infections. Moreover, clubbed roots impede nutrient and water uptake, resulting in leaf discolouration, wilting, and stunted growth (Javed et al., 2023). As a result, crop yield and quality are severely diminished (Strelkov et al., 2016). This issue is widespread in Alberta, affecting approximately 67% of counties and districts, with canola fields being particularly susceptible (Government of Alberta, 2022). Given the economic importance of canola in Alberta, it is imperative to develop effective detection and mitigation strategies for Clubroot. However, current methods for detection and mitigation are limited and unsustainable.

The objective of our project is to create a reliable detection kit and an effective mitigation strategy for Clubroot. In the initial phase of our project, we will focus on developing a robust detection system. This system will enable farmers to identify the presence of pathogenic spores quickly, efficiently, and at a lower cost as compared to existing methods. The development of such a detection kit will greatly benefit farmers by providing timely information and facilitating the implementation of appropriate measures, such as longer crop rotations or equipment sanitization to limit spore spread. The successful creation of a reliable and effective detection kit represents a significant advancement in combating Clubroot. The second phase of our project aims to develop a mitigation system that will be designed to eradicate the presence of Clubroot and offer a sustainable long-term solution. Together, the detection and mitigation systems will work in hand, providing valuable tools for farmers to assist them in proactively managing the disease.

By addressing the urgent need for reliable detection and mitigation strategies, our project will make substantial progress in the fight against Clubroot. Our technology will empower farmers with the knowledge to take appropriate action and establish proactive measures to limit the impact of Clubroot on their crops. Ultimately, the successful development and implementation of our detection and mitigation systems will contribute significantly to preserving the productivity and economic viability of Brassica crops, and particularly canola crops in Alberta.

## References

Javed, M. A., Schweim, A.,, Zamani-Noor, N., Salih, R., Vano, M. S., Wu, J., Gonzalez Garcia, M., Heick, T. M., Luo, C., Prakash, P. & Perez-Lopez, E (2023). The clubroot pathogen Plasmodiophora brassicae: A profile update. Molecular Plant Pathology, 24(2). 89–106.
https://doi.org/10.1111%2Fmpp.13283

Strelkov, S. E., Hwang, S. F., Manolii, V. P., Cao, T., & Feindel, D. (2016). Emergence of new virulence phenotypes of Plasmodiophora brassicae on canola (brassica napus) in Alberta, Canada. European Journal of Plant Pathology, 145(3), 517–529. https://doi.org/10.1007/s10658-016-0888-8

Alberta Clubroot Management Plan. Alberta.ca. (2022). https://www.alberta.ca/alberta-clubroot-management-plan.aspx
