import { defineCollection } from "astro:content"
import { wikiSchema } from "../schemas/wiki"

export const collections = {
    docs: defineCollection({ schema: wikiSchema() }),
}
