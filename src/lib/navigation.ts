import { basename, dirname } from "node:path"
import type { Badge } from "../schemas/badge"
import { routes, type Route } from "./routing"
import { slugToPathname } from "./slugs"
import { pathWithBase } from "./base"
import { ensureTrailingSlash } from "./path"

const DirKey = Symbol("DirKey")

export interface Link {
    type: "link"
    label: string
    href: string
    isCurrent: boolean
    badge: Badge | undefined
}

interface Group {
    type: "group"
    label: string
    entries: (Link | Group)[]
    collapsed: boolean
}

export type SidebarEntry = Link | Group

/**
 * A representation of the route structure. For each object entry:
 * if it’s a folder, the key is the directory name, and value is the directory
 * content; if it’s a route entry, the key is the last segment of the route, and value
 * is the full entry.
 */
interface Dir {
    [DirKey]: undefined
    [item: string]: Dir | Route
}

/** Create a new directory object. */
function makeDir(): Dir {
    const dir = {} as Dir
    // Add DirKey as a non-enumerable property so that `Object.entries(dir)` ignores it.
    Object.defineProperty(dir, DirKey, { enumerable: false })
    return dir
}

/** Check if a string starts with one of `http://` or `https://`. */
const isAbsolute = (link: string) => /^https?:\/\//.test(link)

/** Get the segments leading to a page. */
function getBreadcrumbs(path: string, baseDir: string): string[] {
    // Strip extension from path.
    const pathWithoutExt = stripExtension(path)
    // Index paths will match `baseDir` and don’t include breadcrumbs.
    if (pathWithoutExt === baseDir) return []
    // Ensure base directory ends in a trailing slash.
    if (!baseDir.endsWith("/")) baseDir += "/"
    // Strip base directory from path if present.
    const relativePath = pathWithoutExt.startsWith(baseDir)
        ? pathWithoutExt.replace(baseDir, "")
        : pathWithoutExt
    let dir = dirname(relativePath)
    // Return no breadcrumbs for items in the root directory.
    if (dir === ".") return []
    return dir.split("/")
}

/** Turn a flat array of routes into a tree structure. */
function treeify(routes: Route[], baseDir: string): Dir {
    const treeRoot: Dir = makeDir()
    routes
        // Remove any entries that should be hidden
        .filter((doc) => !doc.entry.data.sidebar?.hidden)
        .forEach((doc) => {
            const breadcrumbs = getBreadcrumbs(doc.id, baseDir)

            // Walk down the route’s path to generate the tree.
            let currentDir = treeRoot
            breadcrumbs.forEach((dir) => {
                // Create new folder if needed.
                if (typeof currentDir[dir] === "undefined")
                    currentDir[dir] = makeDir()
                // Go into the subdirectory.
                currentDir = currentDir[dir] as Dir
            })
            // We’ve walked through the path. Register the route in this directory.
            currentDir[basename(doc.slug)] = doc
        })
    return treeRoot
}

/** Test if the passed object is a directory record.  */
function isDir(data: Record<string, unknown>): data is Dir {
    return DirKey in data
}

/**
 * Get the sort weight for a given route or directory. Lower numbers rank higher.
 * Directories have the weight of the lowest weighted route they contain.
 */
function getOrder(routeOrDir: Route | Dir): number {
    return isDir(routeOrDir)
        ? Math.min(...Object.values(routeOrDir).flatMap(getOrder))
        : // If no order value is found, set it to the largest number possible.
          routeOrDir.entry.data.sidebar?.order ?? Number.MAX_VALUE
}

/** Sort a directory’s entries by user-specified order or alphabetically if no order specified. */
function sortDirEntries(dir: [string, Dir | Route][]): [string, Dir | Route][] {
    const collator = new Intl.Collator()
    return dir.sort(([keyA, a], [keyB, b]) => {
        const [aOrder, bOrder] = [getOrder(a), getOrder(b)]
        // Pages are sorted by order in ascending order.
        if (aOrder !== bOrder) return aOrder < bOrder ? -1 : 1
        // If two pages have the same order value they will be sorted by their slug.
        return collator.compare(
            isDir(a) ? keyA : a.slug,
            isDir(b) ? keyB : b.slug
        )
    })
}

/** Create a group entry for a given content collection directory. */
function groupFromDir(
    dir: Dir,
    fullPath: string,
    dirName: string,
    currentPathname: string,
    collapsed: boolean
): Group {
    const entries = sortDirEntries(Object.entries(dir)).map(
        ([key, dirOrRoute]) =>
            dirToItem(
                dirOrRoute,
                `${fullPath}/${key}`,
                key,
                currentPathname,
                collapsed
            )
    )
    return {
        type: "group",
        label: dirName,
        entries,
        collapsed,
    }
}

/** Create a link entry. */
function makeLink(
    href: string,
    label: string,
    currentPathname: string,
    badge?: Badge
): Link {
    if (!isAbsolute(href)) href = pathWithBase(href)
    const isCurrent = href === ensureTrailingSlash(currentPathname)
    return { type: "link", label, href, isCurrent, badge }
}

/** Create a link entry for a given content collection entry. */
function linkFromRoute(route: Route, currentPathname: string): Link {
    return makeLink(
        slugToPathname(route.slug),
        route.entry.data.sidebar?.label || route.entry.data.title,
        currentPathname,
        route.entry.data.sidebar?.badge
    )
}

/** Create a sidebar entry for a directory or content entry. */
function dirToItem(
    dirOrRoute: Dir[string],
    fullPath: string,
    dirName: string,
    currentPathname: string,
    collapsed: boolean
): SidebarEntry {
    return isDir(dirOrRoute)
        ? groupFromDir(
              dirOrRoute,
              fullPath,
              dirName,
              currentPathname,
              collapsed
          )
        : linkFromRoute(dirOrRoute, currentPathname)
}

/** Create a sidebar entry for a given content directory. */
function sidebarFromDir(
    tree: Dir,
    currentPathname: string,
    collapsed: boolean
) {
    return sortDirEntries(Object.entries(tree)).map(([key, dirOrRoute]) =>
        dirToItem(dirOrRoute, key, key, currentPathname, collapsed)
    )
}

/** Get the sidebar for the current page. */
export function getSidebar(pathname: string): SidebarEntry[] {
    const tree = treeify(routes, "")
    return sidebarFromDir(tree, pathname, false)
}

/** Remove the extension from a path. */
const stripExtension = (path: string) => path.replace(/\.\w+$/, "")
