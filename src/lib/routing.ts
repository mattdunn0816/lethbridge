import type { GetStaticPathsItem } from "astro"
import { type CollectionEntry, getCollection } from "astro:content"
import { slugToParam } from "./slugs"

export type LethbridgeWikiEntry = Omit<CollectionEntry<"wiki">, "slug"> & {
    slug: string
}

export interface Route {
    entry: LethbridgeWikiEntry
    slug: string
    id: string
    isFallback?: true
    [key: string]: unknown
}

interface Path extends GetStaticPathsItem {
    params: { slug: string | undefined }
    props: Route
}

/**
 * Astro is inconsistent in its `index.md` slug generation. In most cases,
 * `index` is stripped, but in the root of a collection, we get a slug of `index`.
 * We map that to an empty string for consistent behaviour.
 */
const normalizeIndexSlug = (slug: string) => (slug === "index" ? "" : slug)

/** All entries in the docs content collection. */
const wiki: LethbridgeWikiEntry[] = (await getCollection("wiki")).map(
    ({ slug, ...entry }) => ({
        ...entry,
        slug: normalizeIndexSlug(slug),
    })
)

function getRoutes(): Route[] {
    const routes: Route[] = wiki.map((entry) => ({
        entry,
        slug: entry.slug,
        id: entry.id,
    }))

    return routes
}
export const routes = getRoutes()

function getPaths(): Path[] {
    return routes.map((route) => ({
        params: { slug: slugToParam(route.slug) },
        props: route,
    }))
}
export const paths = getPaths()
